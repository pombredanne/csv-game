#include <iostream>
#include <iterator>

#include "csvmonkey.hpp"
using namespace csvmonkey;


int main(int argc, char* argv[])
{
    if(argc < 2) {
        return 1;
    }

    MappedFileCursor cursor;
    if(! cursor.open(argv[1])) {
        return 1;
    }

    CsvReader reader(cursor);
    CsvCursor &row = reader.row();
    int sum = 0;
    
    while(reader.read_row()) {
        sum += row.count;
    }
    std::cout << sum << std::endl;
}
