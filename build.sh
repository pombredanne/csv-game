#!/bin/bash

own_timer () {
  local result_file=$1; shift
  local language=$1; shift
  local library=$1; shift
  local task=$1; shift
  TIMEFORMAT="$language,$library,$task,%R"
  for _ in $(seq 1 10); do
    (time eval "$@") 2>> "$result_file"
  done
}

benchmark () {
  local result_file=$1; shift
  local language=$1; shift
  local library=$1; shift
  local task=$1; shift

  local temp_result_file="$result_file".tmp.vsc
  bench --csv "$temp_result_file" "$@"
  echo -n "$language,$library,$task," >> "$result_file"
  tail -n 1 "$temp_result_file" >> "$result_file"
  rm "$temp_result_file"
}

timer () {

  if which bench; then
    benchmark "$@"
  else
    own_timer "$@"
  fi
}
